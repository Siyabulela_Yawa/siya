#include <string>
#include <vector>
#include<iostream>
#include <map>

using namespace std;

int main() {
	vector <string> strings;
	strings.push_back("One");
	strings.push_back("Two");
	strings.push_back("Three");
	strings.push_back("Four");

	vector<string> ::iterator it= strings.begin();
	for (vector<string> ::iterator it = strings.begin(); it != strings.end(); it++) {
		cout << *it<<endl;
	}
	
	map<string, int> ages;
	ages["Mike"] = 40;
	ages["Raj"] = 20;
	ages["Vicky"]= 30;


	cout << ages["Raj"] << endl;
	for (map<string, int> ::iterator it = ages.begin(); it != ages.end(); it++) {
		cout << it->first << ":" << it->second << endl;
	}
}