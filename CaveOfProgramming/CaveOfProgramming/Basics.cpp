#include <iostream>
#include <exception>
/*

using namespace std;
//EXCEPTIONS
class myException : public exception {
public:
	virtual const char* what() const throw() { 
		return "Something bad happened";
	}
};
class Test {
public:
	void goesWrong() {
		throw myException();
	}
};
class canGoWrong {
public: 
	canGoWrong() {
		char* pMemory = new char[99999];
		//delete pMemory;
	}
};
void wrong() {
	bool item = false;

	if (item) {
		throw string("Something went wrong");
	}
}
/*
int main() {
	//EXCEPTIONS
	try {
		//wrong();
		//canGoWrong wrong;
		Test test;
		test.goesWrong();
	}
	catch (string e) {
		cout << e;
	}
	catch (exception& e) {
		cout <<e.what()<<endl;
	}
	cout << "Still running";


	
}
*/