#include <iostream>
#include <WS2tcpip.h>
#include <sstream>

#pragma comment (lib, "ws2_32.lib")

using namespace std;
void main()
{
	//initialise winsock

	WSADATA wsData;
	WORD ver = MAKEWORD(2, 2);   //version that we want (2,2)

	int wsok = WSAStartup(ver, &wsData);   //need to have a pointer to my data when initialising winsock

	if (wsok != 0) {    //if not successful
		cerr << "Can't initialise winsock ! Quitting" << endl;
		return;
	}


	//create socket
	SOCKET listening = socket(AF_INET, SOCK_STREAM, 0);  //socket is just an endpoint it binds a port and an IP address, its just a number

	if (listening == INVALID_SOCKET) {  //testing socket
		cerr << "Cant't create a socket! Quitting" << endl;
		return;
	}

	// Bind ip address and port to a socket 
	sockaddr_in hint;  //hint structure
	hint.sin_family = AF_INET;
	hint.sin_port = htons(54000);
	hint.sin_addr.S_un.S_addr = INADDR_ANY;

	bind(listening, (sockaddr*)&hint, sizeof(hint));


	//Tell winsock the socket is for listening
	listen(listening, SOMAXCONN);   //maximum number of connections listen to, handled by socket

	fd_set master;
	FD_ZERO(&master);

	FD_SET(listening, &master);

	while (true) {
		fd_set copy = master;

		int socketCount = select(0, &copy, nullptr, nullptr, nullptr);

		for (int i = 0; i < socketCount; i++) {
			SOCKET sock = copy.fd_array[i];
			if (sock == listening) {
				//Accept a new connection
				SOCKET client = accept(listening, nullptr, nullptr);
				//add new connection to the list of connected clients
				FD_SET(client, &master);
				//send welcome message to the connected client
				string welcmsg = "Welcome to the chat server\r\n";
				send(client, welcmsg.c_str(), welcmsg.size()+1,0);
				//TODO: Broadcast we have a new connection
			}
			else {
				char buff[4096];
				ZeroMemory(buff, 4096);


				int bytesIn = recv(sock, buff, 4096, 0);

				if (bytesIn <= 0) {
					//drop the client
					closesocket(sock);
					FD_CLR(sock, &master);
				}
				else {
					//Send to other clients and not the listening socket

					for (int i = 0; i < master.fd_count; i++) {
						SOCKET outSock = master.fd_array[i];
						if (outSock != listening && outSock != sock) {
							ostringstream ss;
							ss << "SOCKET #" << sock << ":"<<buff<<"\r\n";
							string strOut = ss.str();
							send(outSock, strOut.c_str(), strOut.size()+1, 0);
						}
					}
				}
				//Accept a new message
				//Send message to other clients, and definatley not the listening socket
			}
		}
	}

	//close socket
//	closesocket(clientSocket);
	//clean up winsock
	WSACleanup();
}
	////wait for connection
	//sockaddr_in client;
	//int clientsize = sizeof(client);

	//SOCKET clientSocket = accept(listening, (sockaddr*)&client, &clientsize);

	//if (clientSocket == INVALID_SOCKET) {
	//	cerr << "Invalid client socket" << endl;
	//	return;
	//}
	//PWCHAR host[NI_MAXHOST];   // client remote name  (might be a port or dns might give us a remote name)
	//PWCHAR service[NI_MAXHOST];  // sERVICE (i.e port) the client is connected on 

	//ZeroMemory(host, NI_MAXHOST);  //same as memset(host,0,NI_MAXHOST)
	//ZeroMemory(service, NI_MAXHOST);
	////memset(host,0,NI_MAXHOST);

	//if (GetNameInfo((sockaddr*)&client, sizeof(client), *host, NI_MAXHOST, *service, NI_MAXSERV, 0) == 0)
	//{
	//	cout << host << " connected on port " << service << endl;
	//}
	//else {
	//	//	inet_ntop(AF_INET, &client.sin_addr, host, NI_MAXHOST);
	//	//	cout << host << "connected on port " << ntohs(client.sin_addr) << endl;
	//}

	////close listening socket
	//closesocket(listening);
	////while loop: accept and echo message back to client

	//char buff[4096];

	//while (true) {
	//	ZeroMemory(buff, 4096);

	//	//wait for client to send data
	//	int bytesRecieved = recv(clientSocket, buff, 4096, 0);
	//	if (bytesRecieved == SOCKET_ERROR) {
	//		cerr << "Error in recv(). Quitting" << endl;
	//		break;
	//	}
	//	if (bytesRecieved == 0) {
	//		cout << "Client disconnected" << endl;
	//		break;
	//	}
	//	cout << buff << endl;



	//	///echo message back to client
	//	send(clientSocket, buff, bytesRecieved + 1, 0);
	//}
