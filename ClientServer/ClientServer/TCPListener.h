#pragma once
#include <iostream>
#include <WS2tcpip.h>   //header file for all winsock functions
#include <string>

#pragma comment(lib, "ws2_32.lib")   //winsock library file

//Forward decalration of class
class TCPListener;
#define MAX_BUFFER_SIZE (4096)
//TODO: Callback to data recieved
typedef void (*MessageRecievedHandler)(TCPListener* listener, int sockId, std::string msg);

using namespace std;
class TCPListener {
private:
	string ip;
	int port;
	MessageRecievedHandler msgRecieved;

public:
	//send msg to specified client
	void Send(int ClientSocket, string Msg);

	//Initialise Socket
	bool Init();

	//Create a socket
	SOCKET createSocket();


	//wait for connection 
	SOCKET waitForConnection(SOCKET listening);

	//main processing loop
	void Run();

	void cleanUp();

	~TCPListener();
	TCPListener(string Ip, int Port, MessageRecievedHandler Handler);
	
};
