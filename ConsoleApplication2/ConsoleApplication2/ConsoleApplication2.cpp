
#include <string>
#include <iostream>

#include <windows.h>
#include <dos.h>
#include <time.h>
#include <thread>

#define MAXFRAMEX 119
#define MAXFRAMEY 29
#define MAXSNAKESIZE 100 


using namespace std;

HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE);
COORD CursorPosition;

void gotoxy(int x, int y) {
    CursorPosition.X = x;
    CursorPosition.Y = y;
    SetConsoleCursorPosition(console,CursorPosition);
}
class Point {

private:
    int x; int y;
public: 
    Point() {
        x = y = 10;
    }
    Point(int X, int Y) {
        x = X;
        y = Y;
    }
    void SetPoint(int X, int Y) {
        x = X;
        y = Y;
    }
    int GetX() {
        return x;
    }
    int GetY() {
        return y;
    }
    void MoveUp() {

        y--;
        if (y < 0)
            y = MAXFRAMEY;
    }
    void MoveDown() {
        y++;
        if (y > MAXFRAMEY)
            y = 0;
    }
    void MoveLeft() {
        x--;
        if (x < 0)
            x = MAXFRAMEX;
    }
    void MoveRight() {
        x++;
        if (x > MAXFRAMEX)
            x = 0;
    }
    void Draw() {
        gotoxy(x, y);
        cout << "*";
       // cout << "5";
    }
    void Erase() {
        gotoxy(x, y);
        cout << " ";
    }void CopyPos(Point* p) {
        p->x = x;
        p->y = y;
    }
    int IsEqual(Point* p) {
        if (p->x == x && p->y == y) {
            return 1;
        }
        return 0;
    }
    void Debug() {
        std::cout << "(" << x << "," << y << ")";
    }
};


class Snake {
private:
    Point* cell[MAXSNAKESIZE];  //array of points to represent snake 
    int size; //current size of snake
    char dir; //current direction of snake
    Point fruit;
    int state; // bool representation shows state of snake i.e Living or dead
    int started;
    int score;
public:
    Snake() {
        size = 1; //default size
        cell[0] = new Point(20, 20);  //default position
        for (int i = 1; i < MAXSNAKESIZE < size; i++) {
            cell[i] = NULL;
        }
        fruit.SetPoint(rand() % MAXFRAMEX, rand() % MAXFRAMEY);  //
        state = 0;
        score = 0;
        Play();
    }
    void AddCell(int x, int y) {
        cell[size++] = new Point(x, y);

    }
    void TurnUp() {
        if(dir!='s')
        dir = 'w';  //w is controling key for turning upwards
    }
    void TurnDown() {
        if (dir != 'w')
        dir = 's';  //s is controling key for turning downwards
    }
    void TurnLeft() {
        if (dir != 'd')
        dir = 'a';  //a is controling key for turning left
    }
    void TurnRight() {
        if (dir != 'a')
        dir = 'd';  //d is controling key for turning right
    }
    void WelcomeScreen() {
        cout << "WELCOME TO SNAKE";
    }
    void getch(char& op) {

        DWORD mode, cc;
        cout << score;
        HANDLE h = GetStdHandle(STD_INPUT_HANDLE);
        if (h == NULL) {
            return; // console not found
        }
        GetConsoleMode(h, &mode);
        SetConsoleMode(h, mode & ~(ENABLE_LINE_INPUT | ENABLE_ECHO_INPUT));

        ReadConsole(h, &op, 1, &cc, NULL);
        // SetConsoleMode(h, mode);
        // return c;d
    }
    void Move() {
        //clear screen 
        system("cls");

        if (state == 0) {
            if (started) {
                WelcomeScreen();
                cout << "Press any key to start";
                string quit;
                cin >> quit;
                //getch();
                state = 1;
                size = 1;
            }
            else {
                cout << "Game Over";
                cout << "Press any key to start";
                string quit;
                cin >> quit;
                //getch();
                state = 1;
                size = 1;
            }
            
        }
        //making snake body follow its head
        for (int i = size - 1; i > 0; i--) {
            cell[i-1]->CopyPos(cell[i]);
        }

        //turning snake's head
        switch (dir) {
        case 'w': TurnUp();
            cell[0]->MoveUp();
            break;
        case 's': TurnDown();
            cell[0]->MoveDown();
            break;
        case 'a': TurnLeft();
            cell[0]->MoveLeft();
            break;
        case 'd': TurnRight();
            cell[0]->MoveRight();
            break;
        }
        if (SelfCollision()) {
            state = 0;
            size = 1;
            score = 0;

        }
        // Collision with fruit Point
        if (fruit.GetX() == cell[0]->GetX() && fruit.GetY() == cell[0]->GetY()) {
           
            fruit.SetPoint(rand() % MAXFRAMEX, rand() % MAXFRAMEY);  // 
            AddCell(fruit.GetX(), fruit.GetY());
            score++;
        }
        //drawing snake
        for (int i = 0; i < size; i++) 
            cell[i]->Draw();
        fruit.Draw();
      //  Debug();
        Sleep(50);
        
    }
    int SelfCollision() {
        for (int i = 1; i < size; i++) {
            if (cell[0]->IsEqual(cell[i])) {
                return 1;
            }

        }
        return 0;
    }
    void Debug() {
        for (int i = 0; i < size; i++) {
            cell[i]->Debug();
        }
    }
    void Play() {
        char op = 'l';
        char* opptr = &op;
  
        do {

            getch(op);
            switch (op) {
            case 'w': TurnUp();
            case 'W':TurnUp(); break;
            case 's': TurnDown();
            case 'S':TurnDown(); break;
            case 'a': TurnLeft();
            case 'A':TurnLeft(); break;
            case 'd': TurnRight();
            case 'D':TurnRight(); break;
            }

            Move();
        } while (op != 'e');
    }
};

int main()
{
    //rando no generation 
    srand((unsigned)time(NULL));

    //Testing snake
    Snake snake;
   
    Point p(5, 20);
    p.Draw();
 //   std::cout << "Hello World!\n";
 
   


}


