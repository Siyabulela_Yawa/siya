#include <iostream>
#include <WS2tcpip.h>
#include <string>

#pragma comment(lib, "ws2_32.lib")

using namespace std;

void main() {
	string ip = "192.168.3.85";   //ip of server
	int port = 54000;   //listening port on the server

	// Initialise winsock
	WSAData data;
	WORD ver = MAKEWORD(2, 2);
	int wsResult = WSAStartup(ver, &data);
	if (wsResult != 0) {  //0 no error
		cerr << "Can't start winsock error number " << wsResult << endl;
		return;
	}
	// create socket socket - this is the identifier our program will use to comunicate with the netwotwork layer to send data to the remote machine

	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);
	if (sock == INVALID_SOCKET) {
		cerr << "Can't create socket, Err # " << WSAGetLastError() << endl;
	}


	// Fill in a hint structure
	sockaddr_in hint;
	hint.sin_family = AF_INET;
	hint.sin_port = htons(port);
	inet_pton(AF_INET, ip.c_str(), &hint.sin_addr);


	// Connect to the server
	int connResult = connect(sock, (sockaddr*)&hint, sizeof(hint));
	if (connResult == SOCKET_ERROR) {
		cerr << " Can't connect to server " << WSAGetLastError() << endl;
		closesocket(sock);
		WSACleanup();

		return;
	}

	//do while lopp to sebd and recieve data

	char buff[4096];
	string userinput;

	do {
		//Promt the user for sime text
		cout << "> ";
		getline(cin, userinput);

		if (userinput.size() > 0) {
			//Send the text
			int sendResult = send(sock, userinput.c_str(), userinput.size() + 1, 0); //have a trailing zero for every string
			
			if (sendResult != SOCKET_ERROR) {
				//Wait for response
				ZeroMemory(buff, 4096);
				int bytesRecieved = recv(sock, buff, 4096, 0);
				if (bytesRecieved > 0) {
					//Echo response 
					cout << "SERVER> " << string(buff, 0, bytesRecieved) << endl;
				}
				
			}

		
		}
		
	} while (userinput.size() > 0);
	
	//close down everything
	closesocket(sock);
	WSACleanup();
}